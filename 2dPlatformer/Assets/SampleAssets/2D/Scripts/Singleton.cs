﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	private static T _instance;

	private static object _lock = new object();

	void Awake(){
	}

	private void CreateSingleton(){
		GameObject singleton = new GameObject();
		_instance = singleton.AddComponent<T>();
		singleton.name = "(singleton) "+ typeof(T).ToString();

		//DontDestroyOnLoad(singleton);
	}

	public static T Instance
	{
		get{
			lock(_lock){
				if (_instance == null){
					_instance = (T) FindObjectOfType(typeof(T));

					if ( FindObjectsOfType(typeof(T)).Length > 1 )
						return _instance;

					if (_instance == null){
						GameObject singleton = new GameObject();
						_instance = singleton.AddComponent<T>();
						singleton.name = "(singleton) "+ typeof(T).ToString();
						//DontDestroyOnLoad(singleton);
					} else {
						//DontDestroyOnLoad(_instance.gameObject);
					}
				}

				return _instance;
			}
		}
	}

	private static bool applicationIsQuitting = false;
	public void OnDestroy () {
		applicationIsQuitting = true;
	}
}
