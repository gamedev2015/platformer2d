﻿using UnityEngine;
using System.Collections;

public class MoveTrail : MonoBehaviour {

	public int moveSpeed;
	private float destroyTime = 1;

	// Update is called once per frame
	void Update () {
		transform.Translate (Vector2.right * Time.deltaTime * moveSpeed);
		GameObject.Destroy (this.gameObject, destroyTime);
	}
}
