﻿using UnityEngine;
using System;
using System.Collections;
using UnitySampleAssets._2D;

[RequireComponent(typeof(Platformer2DUserControl))]
public class Player : MonoBehaviour {

	[SerializeField] StatusIndicator statusIndicator;

	private PlayerStat stats;

	void Awake() {
		stats = PlayerStat.instance;
		stats.CurrentHealth = stats.maxHealth;
		statusIndicator.SetHealth (stats.CurrentHealth, stats.maxHealth);
		GameMaster.Instance.upgradeMenuCallback += ToggleUpgradeMenu;
	}

	void Start () {
		InvokeRepeating ("RegenHealth", stats.regenRate, stats.regenRate);
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (LayerMask.LayerToName(coll.gameObject.layer) == "VoidCollider")
			DamagePlayer (int.MaxValue);
	}

	public void ToggleUpgradeMenu(bool active) {
		if (this != null) { 
			GetComponent<Platformer2DUserControl> ().enabled = !active;
			Weapon weapon = GetComponentInChildren<Weapon> ();
			if (weapon != null) {
				weapon.enabled = !active;
			}
		}
	}

	public void RegenHealth () {
		stats.CurrentHealth += 1;
		statusIndicator.SetHealth (stats.CurrentHealth, stats.maxHealth);
	}

	 public void DamagePlayer (int damage) {
		stats.CurrentHealth -= damage;
		if (stats.CurrentHealth <= 0) {
			GameMaster.Instance.KillPlayer (this);
			AudioManager.instance.PlaySound ("Die");
		} else {
			AudioManager.instance.PlaySound ("Grunt");
			statusIndicator.SetHealth (stats.CurrentHealth, stats.maxHealth);
		}
	}
}
