﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class WaveUI : MonoBehaviour {

	[SerializeField] private WaveSpawner spawner;
	[SerializeField] private Text waveCountdownText;
	[SerializeField] private Text waveCountText;

	private Animator animator;
	private WaveSpawner.SpawnState previousState;

	void Awake () {
		animator = GetComponent<Animator> ();
	}
	 

	// Use this for initialization
	void Update () {
		switch (spawner.State) {
			case WaveSpawner.SpawnState.Counting:
				UpdateCountingUI ();
				break;

			case WaveSpawner.SpawnState.Spawning:
				UpdateSpawningUI ();
				break;
		}

		previousState = spawner.State;
	}

	private void UpdateCountingUI () {
		if (previousState != WaveSpawner.SpawnState.Counting) {
			animator.SetBool ("WaveIncoming", false);
			animator.SetBool ("WaveCountdown", true);
		}
		waveCountdownText.text = Mathf.FloorToInt (spawner.WaveCountDown).ToString ();
	}

	private void UpdateSpawningUI () {
		if (previousState != WaveSpawner.SpawnState.Spawning) {
			animator.SetBool ("WaveIncoming", true);
			animator.SetBool ("WaveCountdown", false);
			waveCountText.text = spawner.NextWave.ToString ();
		}
	}
	

}
