﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {

	void Awake () {
		SceneManager.LoadScene ("Common", LoadSceneMode.Additive);
	}

	void Start () {
		AudioManager.instance.PlaySound ("Music");
	}

	public void StartGame () {
		AudioManager.instance.PlaySound ("Press");
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1);
	}

	public void Quit () {
		AudioManager.instance.PlaySound ("Press");
		Application.Quit ();
	}

	public void Hover () {
		AudioManager.instance.PlaySound ("Hover");
	}

}
