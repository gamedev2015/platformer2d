using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Text))]
public class LivesCounterUI : MonoBehaviour {

	private Text livesText;
	private const string livesStr = "LIVES {0}";

	void Awake() {
		livesText = GetComponent<Text> ();
	}

	public void Notify (int lives) {
		livesText.text = string.Format (livesStr, lives);
	}


}
