﻿using UnityEngine;
using System.Collections;

public class PlayerStat : MonoBehaviour {

	public static PlayerStat instance;
	public int maxHealth = 100;

	public float regenRate = 2f;
	public float maxSpeed = 10f;

	private int _currentHealth;
	public int CurrentHealth {
		get { 
			return _currentHealth;
		}
		set { 
			_currentHealth = Mathf.Clamp (value, 0, maxHealth);
		}
	}

	void Awake () {
		if (instance == null) {
			instance = this;
		}
		//_currentHealth = maxHealth;
	}
		
}
