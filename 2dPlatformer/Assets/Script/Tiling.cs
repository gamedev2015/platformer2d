﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]

public class Tiling : MonoBehaviour {

	public int offset = 2;

	private bool rightInstantied;
	public bool RightInstantied {
		set { 
			rightInstantied = value;
		}
	}

	private bool leftInstantied;
	public bool LeftInstantied {
		set { 
			leftInstantied = value;
		}
	}

	private bool reverseScale;
	private Transform myTransform;
	private SpriteRenderer sprite;
	private float spriteWidth;
	private float cameraHorizontalExtend;
	private Camera cam;

	void Awake () {
		sprite = GetComponent<SpriteRenderer>();
		myTransform = transform;
		cam = Camera.main;
	}

	void Start () {
		cameraHorizontalExtend = (cam.orthographicSize) * Screen.width / Screen.height;
		spriteWidth = sprite.bounds.size.x;
	}
	
	void Update () {
		if (!rightInstantied && !leftInstantied) {
			float rightLimit = myTransform.localPosition.x + spriteWidth / 2;
			float leftLimit = myTransform.localPosition.x - spriteWidth / 2;

			float farRightVisible = cam.transform.position.x + cameraHorizontalExtend;
			float farLeftVisible = cam.transform.position.x - cameraHorizontalExtend;

			if ((rightLimit - farRightVisible) < offset && !rightInstantied) {
				Add (true);
			}

			if ((farLeftVisible - leftLimit) < offset && !leftInstantied) {
				Add (false);
			}
		}
	}

	void Add(bool right) {
		Transform newElement = Instantiate(myTransform) as Transform;
		newElement.SetParent(myTransform.parent ,false);
		newElement.position = new Vector3 (myTransform.position.x + spriteWidth * (right ? 1 : -1), myTransform.position.y, myTransform.position.z);
		if (reverseScale) {
			newElement.localScale = new Vector3 (myTransform.localScale.x * -1, myTransform.localScale.y, myTransform.localScale.z);
		}
		rightInstantied = right;
		leftInstantied = !right;

		newElement.GetComponent<Tiling> ().LeftInstantied = right;
		newElement.GetComponent<Tiling> ().rightInstantied = !right;
	}
}
