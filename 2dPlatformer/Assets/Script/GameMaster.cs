﻿using UnityEngine;
using System;
using System.Collections;
using UnitySampleAssets._2D;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(WaveSpawner))]
public class GameMaster : Singleton<GameMaster> {

	protected GameMaster () {}

	private WaveSpawner waveSpawner;

	private AudioManager audioManager;

	[SerializeField] private GameObject gameOverUI;
	[SerializeField] private LivesCounterUI livesCounter;
	[SerializeField] private MoneyCounterUI moneyCounter;
	[SerializeField] private Transform playerPrefab;
	[SerializeField] private Transform spawnPosition;
	[SerializeField] private Transform spawnEffectPrefab;
	[SerializeField] private GameObject upgradeMenu;

	public delegate void UpgradeMenuCallback(bool active);
	public UpgradeMenuCallback upgradeMenuCallback;

	[SerializeField] private CameraShake cameraShake;
	public CameraShake CameraShake {
		get { 
			return cameraShake;
		}
	}

	[SerializeField] private Transform garbageCollector;
	public Transform GarbageCollector {
		get { 
			return garbageCollector;
		}
	}

	[SerializeField]
	private int startMoney = 100;
	public static int Money;

	private Transform player;
	public Transform Player {
		get { 
			return player;
		}
	}

	private Camera2DFollow mainCameraInstance;
	private AudioSource audioSource;

	private int _remainingLives;
	public int RemainingLives {
		get { 
			return _remainingLives;
		}

	}

	void Awake () {
		//SceneManager.LoadScene ("Common", LoadSceneMode.Additive);
		Money = startMoney;
	}

	void Start () {
		audioManager = AudioManager.instance;
		_remainingLives = 3;
		waveSpawner = GetComponent<WaveSpawner> ();
		player = Instantiate (playerPrefab, spawnPosition.position, spawnPosition.rotation) as Transform;
		GetCamera2DFollow().SetTarget (player);
		GetCamera2DFollow ().Init ();
		livesCounter.Notify (RemainingLives);
		moneyCounter.Notify (Money);
	}

	private Camera2DFollow GetCamera2DFollow() {
		if (object.ReferenceEquals(null, mainCameraInstance)) {
			mainCameraInstance = Camera.main.transform.parent.GetComponent<Camera2DFollow> ();
		}
		return mainCameraInstance;
	}

	private AudioSource GetAudioSource () {
		if (object.ReferenceEquals(null, audioSource)) {
			audioSource = this.GetComponent<AudioSource> ();
		}
		return audioSource;
	}

	public void RespawnPosition () {
		player = Instantiate (playerPrefab, spawnPosition.position, spawnPosition.rotation) as Transform;
		GetCamera2DFollow().SetTarget (player);
		AudioManager.instance.PlaySound ("Respawn2");
		waveSpawner.NotifyPlayer (player);

		Transform go = Instantiate (spawnEffectPrefab, spawnPosition.position, spawnPosition.rotation) as Transform;
		GameObject.Destroy (go.gameObject, 3f);
	}

	public void KillPlayer (Player player) {
		Destroy (player.gameObject);
		--_remainingLives;
		livesCounter.Notify (RemainingLives);
		if (_remainingLives <= 0) {
			EndGame ();
		} else {
			audioManager.PlaySound ("Respawn");
			// ().Play ();
			Invoke (((Action)RespawnPosition).Method.Name, 4f);
		}
	}

	public void Update() {
		if (Input.GetKeyDown(KeyCode.U)) {
			ToggleUpgradeMenu ();
		}
	}

	private void ToggleUpgradeMenu() {
		upgradeMenu.SetActive (!upgradeMenu.activeSelf);
		upgradeMenuCallback.Invoke (upgradeMenu.activeSelf);
		waveSpawner.enabled = !upgradeMenu.activeSelf;
		moneyCounter.Notify (Money);
	}

	private void EndGame() {
		AudioManager.instance.PlaySound ("Over");
		gameOverUI.SetActive (true);
	}

	public void KillEnemy (Enemy enemy) {

		AudioManager.instance.PlaySound ("Explosion");

		Transform part = Instantiate (enemy.enemyDeathParticuleSystem, enemy.transform.position, Quaternion.identity) as Transform;
		part.SetParent (GarbageCollector, true);
		part.gameObject.SetActive (true);
		GameObject.Destroy (part.gameObject, 3f);
		CameraShake.Shake (enemy.shakeAmt, enemy.shakeLenght);

		Money += enemy.moneyDrop;
		moneyCounter.Notify (Money);
		AudioManager.instance.PlaySound ("Money");

		Destroy (enemy.gameObject);
		waveSpawner.NotifyEnemy ();
	}

}
