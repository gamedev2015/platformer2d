﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(EnemyAI))]
public class Enemy : MonoBehaviour {

	[Serializable]
	public class EnemyStats {
		public int maxHealth;

		private int _currentHealth;
		public int CurrentHealth {
			get { 
				return _currentHealth;
			}
			set { 
				_currentHealth = Mathf.Clamp (value, 0, maxHealth);
			}
		}

		public int amountDamage = 20;

		public EnemyStats (int _maxHealth) {
			_currentHealth = maxHealth = _maxHealth;
		}
	}

	public Transform enemyDeathParticuleSystem;
	public EnemyStats enemyStats;

	public float shakeAmt = 0.1f;
	public float shakeLenght = 0.1f;
	public int moneyDrop = 10;

	[Header("Optionnal: ")]
	[SerializeField]
	private StatusIndicator statusIndicator;

	void Start () {
		enemyStats = new EnemyStats(100);
		GameMaster.Instance.upgradeMenuCallback += ToggleUpgradeMenu;
		statusIndicator.SetHealth (enemyStats.CurrentHealth, enemyStats.maxHealth);
	}

	public void DamageEnemy (int damage) {
		enemyStats.CurrentHealth -= damage;
		if (enemyStats.CurrentHealth <= 0) {
			GameMaster.Instance.KillEnemy (this);
		} else {
			statusIndicator.SetHealth (enemyStats.CurrentHealth, enemyStats.maxHealth);
		}
	}

	public void ToggleUpgradeMenu(bool active) {
		GetComponent<EnemyAI> ().enabled = !active;
	}

	void OnCollisionEnter2D (Collision2D _colInfo) {
		Player player = _colInfo.collider.GetComponent<Player> ();
		if (player != null ) {
			player.DamagePlayer (enemyStats.amountDamage);
			DamageEnemy (99999);
		} 
	}

	void OnDestroy() {
		GameMaster.Instance.upgradeMenuCallback -= ToggleUpgradeMenu;
	}
}
