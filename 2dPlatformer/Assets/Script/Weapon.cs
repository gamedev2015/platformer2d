﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

	public float fireRate = 0;
	public int damage = 0;
	public LayerMask whatToHit;

	private float timeToSpawnEffect = 0;
	public float effectSpawnRate = 10;

	private float timeToFire = 0;
	[SerializeField] private Transform firePoint;
	[SerializeField] private LineRenderer bulletTrailPrefab;
	private Transform garbageCollector;
	[SerializeField] private Transform MuzzleEffectPrefab;
	[SerializeField] private Transform hitParticulePrefab;

	[SerializeField] private float camShakeAmt = 0.05f;
	[SerializeField] private float camShakeLength = 0.1f;

	private readonly Vector3 noCollisionVector = new Vector3 (int.MaxValue, int.MaxValue, int.MaxValue);

	void Start() {
		garbageCollector = GameMaster.Instance.GarbageCollector;
	}

	void Update () {
		if (fireRate == 0) {
			if (Input.GetButtonDown ("Shoot")) {
				Shoot ();
			}
		}
		else {
			if (Input.GetButton ("Shoot") && Time.time > timeToFire) {
				timeToFire = Time.time + 1/fireRate;
				Shoot ();
			}
		}
	}

	private void Shoot() {
		float mouseWorldPositionX = Camera.main.ScreenToWorldPoint (Input.mousePosition).x;
		float mouseWorldPositionY = Camera.main.ScreenToWorldPoint (Input.mousePosition).y;
		Vector2 mousePosition = new Vector2 (mouseWorldPositionX, mouseWorldPositionY);

		Vector2 origin = new Vector2 (firePoint.position.x, firePoint.position.y);
		Vector2 direction = mousePosition - origin;
		RaycastHit2D hit = Physics2D.Raycast (origin, direction, 100, whatToHit);
	
		if (hit.collider != null) {
			Debug.DrawLine (origin, hit.point, Color.red);

			Enemy enemy = hit.collider.GetComponent<Enemy> ();
			if (enemy != null) {
				enemy.DamageEnemy (damage);
				Debug.Log ("hit " + hit.collider.name);
			}
		}

		if (Time.time > timeToSpawnEffect) {
			Vector3 hitPos;
			Vector3 hitNormal;

			if (hit.collider == null) {
				hitPos = direction * 30;
				hitNormal = noCollisionVector;
			} 
			else {
				hitPos = hit.point;
				hitNormal = hit.normal;
			}

			Effect (hitPos, hitNormal);
			timeToSpawnEffect = Time.time + 1 / effectSpawnRate;
		}
		//Debug.DrawLine (origin, direction*100, Color.cyan);
	}

	private void Effect(Vector3 hitPos, Vector3 hitNormal) {
		LineRenderer lr = Instantiate (bulletTrailPrefab, firePoint.position, firePoint.rotation) as LineRenderer;
		lr.transform.SetParent (garbageCollector, true);

		lr.SetPosition(0, firePoint.position);
		lr.SetPosition(1, hitPos);
		GameObject.Destroy (lr.gameObject, 0.05f);

		if (hitNormal != noCollisionVector) {
			Transform impact = Instantiate (hitParticulePrefab, hitPos, Quaternion.FromToRotation(Vector3.right, hitNormal)) as Transform;
			impact.SetParent (garbageCollector, true);
			//Debug.DrawLine (hitPos, hitNormal*100, Color.cyan);
			GameObject.Destroy (impact.gameObject,1);
		}

		Transform muzzle = Instantiate (MuzzleEffectPrefab, firePoint.position, firePoint.rotation) as Transform;
		muzzle.SetParent (garbageCollector, true);

		float size = Random.Range (0.6f, 0.9f);
		muzzle.localScale = new Vector2 (size, size);
		GameObject.Destroy (muzzle.gameObject, 0.02f);

		GameMaster.Instance.CameraShake.Shake (camShakeAmt, camShakeLength);

		AudioManager.instance.PlaySound ("Shot");
	}

}

