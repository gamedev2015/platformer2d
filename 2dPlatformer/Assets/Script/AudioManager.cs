﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Sound {

	public string name;
	public AudioClip audioClip;
	private AudioSource _audioSource;

	[Range(0f, 1f)]
	public float volume = 0.7f;
	[Range(0.5f, 1.5f)]
	public float pitch = 1f;

	[Range(0f, 0.5f)]
	public float randomVolume = 0.1f;
	[Range(0f, 0.5f)]
	public float randomPitch = 0.1f;

	public bool loop = false;

	public void SetSource (AudioSource _source) {
		_audioSource = _source;
		_audioSource.clip = audioClip;
		_audioSource.loop = loop;
	}

	public void Play () {
		_audioSource.volume = volume * (1 + Random.Range(-randomVolume / 2, randomVolume / 2));
		_audioSource.pitch =  pitch * (1 + Random.Range(-randomVolume / 2, randomVolume / 2));
		_audioSource.Play ();
	}

	public void Stop () {
		_audioSource.Stop ();
	}
}

public class AudioManager : MonoBehaviour {

	public static AudioManager instance;


	[SerializeField] private Sound[] sounds;

	void Awake () {
		if (instance != null) {
			if (instance != this) {
				Destroy (this.gameObject);
			}
			//Debug.LogWarning (" AudioManager not singleton ");
		} else {
			DontDestroyOnLoad (instance);
			instance = this;
			Init ();
		}

		DontDestroyOnLoad (this.gameObject);
	}

	void Init () {
		for (int i = 0; i < sounds.Length; i++) {
			GameObject _go = new GameObject ("Sound_" + i + "_" + sounds[i].name); 
			_go.transform.SetParent (this.transform, false);
			sounds [i].SetSource (_go.AddComponent<AudioSource>());
		}
	}

	public void PlaySound (string _name) {
		for (int i = 0; i < sounds.Length; i++) {
			if (sounds[i].name == _name) {
				sounds [i].Play ();
				return;
			}
		}
		Debug.LogWarning (" sound not found " + _name);
	}

	public void StopSound (string _name) {
		for (int i = 0; i < sounds.Length; i++) {
			if (sounds[i].name == _name) {
				sounds [i].Stop();
				return;
			}
		}
		Debug.LogWarning (" sound not found " + _name);
	}

}
