﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StatusIndicator : MonoBehaviour {

	[SerializeField] private RectTransform hpBar;
	[SerializeField] private Text hpText;

	public void SetHealth(int _curr, int _max) {
		float _value = (float)_curr / _max;

		hpBar.localScale = new Vector3 (_value, hpBar.localScale.y, hpBar.localScale.z);
		hpText.text = "HP: " + _curr + "/" + _max;
	}
}
