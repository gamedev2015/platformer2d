﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameOverUI : MonoBehaviour {

	public void Quit () {
		AudioManager.instance.PlaySound ("Press");
		Application.Quit ();
	}

	public void Retry () {
		AudioManager.instance.PlaySound ("Press");
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
	}

	public void Hover () {
		AudioManager.instance.PlaySound ("Hover");
	}
}
