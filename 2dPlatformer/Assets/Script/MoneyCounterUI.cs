﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Text))]
public class MoneyCounterUI : MonoBehaviour {

	private Text moneyText;
	private const string moneyStr = "Money {0}";

	void Awake() {
		moneyText = GetComponent<Text> ();
	}

	public void Notify (int amount) {
		moneyText.text = string.Format (moneyStr, amount);
	}


}
