﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class WaveSpawner : MonoBehaviour {

	private List<EnemyAI> enemies;

	public enum SpawnState {
		Spawning,
		Waiting,
		Counting
	}

	[Serializable]
	public class Wave {
		public string name;
		public EnemyAI enemy;
		public int count;
		public float rate;
	}

	public Transform[] spawnPoint;
	public Wave[] waves;
	private int nextWave = 0;
	public int NextWave {
		get { 
			return nextWave;
		}
	}

	public float timeBetweenWaves = 5;
	private float waveCountDown;
	public float WaveCountDown {
		get { 
			return Mathf.Clamp(waveCountDown, 0f, timeBetweenWaves);
		}
	}

	private SpawnState state;
	public SpawnState State {
		get { 
			return state;
		}
	}

	private int enemyAliveCount;

	void Start () {
		enemies = new List<EnemyAI> ();
		state = SpawnState.Counting;
		waveCountDown = timeBetweenWaves;
	}

	void Update () {
		if (state == SpawnState.Waiting) {
			if (!IsEnemyAlive ()) {
				WaveCleared ();
			} else {
				return;
			}
		}
			
		if (waveCountDown <= 0) {
			if (state != SpawnState.Spawning) {
				StartCoroutine(SpawnWave(waves[nextWave]));
			} 
		} else {
			waveCountDown -= Time.deltaTime;
		}
	}

	void WaveCleared () {
		Debug.Log ("wave cleared");
		waveCountDown = timeBetweenWaves;
		state = SpawnState.Counting;
	
		if (nextWave + 1 > waves.Length - 1) {
			nextWave = 0;
			Debug.Log ("ALL WAVES CLEARED");
		} else {
			nextWave++;
		}
			
	}

	private bool IsEnemyAlive () {
		return enemyAliveCount > 0;
	}

	IEnumerator SpawnWave (Wave _wave) {
		state = SpawnState.Spawning;
		for (int i = 0; i < _wave.count; i++) {
			SpawnEnemy (_wave.enemy);
			yield return new WaitForSeconds (1f/_wave.rate);
		}
		state = SpawnState.Waiting;
		yield break;
	}

	public void NotifyPlayer(Transform player) {
		foreach (EnemyAI eai in enemies) {
			eai.SetTarget (player);
		}
	}

	public void NotifyEnemy() {
		enemyAliveCount--;
	}


	void SpawnEnemy (EnemyAI _enemy) 
	{
		Debug.Log ("enemy name: " + _enemy.gameObject.name);
		Transform sp = spawnPoint [UnityEngine.Random.Range (0, spawnPoint.Length)];
		EnemyAI e = Instantiate (_enemy, sp.position, sp.rotation) as EnemyAI;
		e.SetTarget (GameMaster.Instance.Player);
		enemies.Add (e);
		enemyAliveCount++;
	}
}
