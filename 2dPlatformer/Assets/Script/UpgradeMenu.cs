﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UpgradeMenu : MonoBehaviour {

	[SerializeField]
	private Text healthText;

	[SerializeField]
	private Text speedText;

	[SerializeField]
	private float healthMultiplier = 1.3f;

	[SerializeField]
	private float SpeedMultiplier = 1.2f;

	[SerializeField]
	private int upgradeCost = 50;

	private PlayerStat stats;

	void OnEnable () {
		stats = PlayerStat.instance;
		UpdateValues ();
	}

	void UpdateValues () {
		healthText.text = "HEALTH: " + stats.maxHealth.ToString ();
		speedText.text = "SPEED: " + stats.maxSpeed.ToString ();
	}

	public void UpgrageHealth() {
		if (GameMaster.Money < upgradeCost) {
			AudioManager.instance.PlaySound ("NoMoney");
			return;
		}

		stats.maxHealth = (int) (healthMultiplier * stats.maxHealth);
		GameMaster.Money -= upgradeCost;
		AudioManager.instance.PlaySound ("Money");
		UpdateValues ();
	}

	public void UpgrageSpeed() {
		if (GameMaster.Money < upgradeCost) {
			AudioManager.instance.PlaySound ("NoMoney");
			return;
		}

		stats.maxSpeed = Mathf.Round(SpeedMultiplier * stats.maxSpeed);
		GameMaster.Money -= upgradeCost;
		AudioManager.instance.PlaySound ("Money");
		UpdateValues ();
	}
}
