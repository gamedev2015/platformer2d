﻿using UnityEngine;
using System;
using System.Collections;
using Pathfinding;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Seeker))]
public class EnemyAI : MonoBehaviour {

	private Transform target;
	[SerializeField] float updateRate = 2f;

	private Seeker seeker;
	private Rigidbody2D rigidBody2D;

	private Path path;
	public float speed = 300f;
	public ForceMode2D forceMode2D;

	private bool pathIsEnded = false;
	public float nextWaypointDistance = 3;
	private int currentWayPoint = 0;

	void Awake () {
		seeker = GetComponent<Seeker> ();
		rigidBody2D = GetComponent<Rigidbody2D> ();
	}

	void Start () {
		if (target == null)
			return;

		InvokeRepeating (((Action)UpdatePath).Method.Name, 0f, 1f/updateRate);
		seeker.StartPath (transform.position, target.position, OnPathComplete);
	}

	public void SetTarget(Transform tr) {
		target = tr;
	}

	public void UpdatePath () {
		if (target == null) {
			return;
		}

		seeker.StartPath (transform.position, target.position, OnPathComplete);
	}
		
	public void OnPathComplete(Path p) {
		//Debug.Log ("Path completed");
		if (!p.error) {
			path = p;
			currentWayPoint = 0;
		} else {
			Debug.Log ("Path errors " + p.error);
		}
	}

	void FixedUpdate () {
		if (target == null || path == null) 
			return;

		if (currentWayPoint >= path.vectorPath.Count) {
			if (pathIsEnded)
				return;

			//Debug.Log ("End of path");
			pathIsEnded = true;
			return;
		} else {
			pathIsEnded = false;
			Vector3 dir = (path.vectorPath [currentWayPoint] - transform.position).normalized;
			dir *= speed * Time.fixedDeltaTime;
			rigidBody2D.AddForce (dir, forceMode2D);

			float dist = Vector2.Distance (transform.position, path.vectorPath[currentWayPoint]);
			if (dist < nextWaypointDistance) {
				currentWayPoint++;
			}
		}
	}
}
