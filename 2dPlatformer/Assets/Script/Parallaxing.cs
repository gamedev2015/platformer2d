using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class Parallaxing : MonoBehaviour {

	[SerializeField] private Transform[] parallaxedObject;

	private float smoothing = 1;
	private Transform cam;
	private Vector3 previousCamPosition;

	private Dictionary<Transform, float> parallaxMap;

	void Awake() {
		cam = Camera.main.transform;
	}

	void Start () {
		previousCamPosition = cam.position;
		parallaxMap = parallaxedObject.ToDictionary(key => key, value => value.transform.position.z);
	}
	
	void Update () {
		foreach (KeyValuePair<Transform, float> entry in parallaxMap) {
			float parallax = (cam.position.x - previousCamPosition.x) * entry.Value;
			float targetPositionX = entry.Key.position.x + parallax;

			Vector3 tmp = entry.Key.position;
			tmp.x = targetPositionX;
			entry.Key.position = Vector3.Lerp (entry.Key.position, tmp, smoothing * Time.deltaTime);
		}
		previousCamPosition = cam.position;
	}
}
