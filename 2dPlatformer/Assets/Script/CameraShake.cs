﻿using UnityEngine;
using System.Collections;
using System;

public class CameraShake : MonoBehaviour {

	private Camera mainCam;
	float shakeAmount = 1.0f;
	const float shakeLenght = 1.0f;

	void Awake () {
		mainCam = Camera.main;
	}

	public void Shake (float amt, float lenght = shakeLenght) 
	{
		shakeAmount = amt;
		InvokeRepeating (((Action)DoShake).Method.Name, 0f, 0.01f);
		Invoke (((Action)StopShake).Method.Name, lenght);
	}

	private void DoShake () {
		if (shakeAmount > 0) {
			Vector3 camPos = mainCam.transform.position;
			float offsetX = UnityEngine.Random.value * shakeAmount * 2 - shakeAmount;
			float offsetY = UnityEngine.Random.value * shakeAmount * 2 - shakeAmount;
			camPos.x += offsetX;
			camPos.y += offsetY;
			mainCam.transform.position = camPos;
		}
	}

	private void StopShake () {
		CancelInvoke (((Action)DoShake).Method.Name);
		mainCam.transform.transform.localPosition = Vector3.zero;
	}
}
